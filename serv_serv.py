#-*-coding:utf-8-*-
import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(3,GPIO.OUT)
Servo=GPIO.PWM(3,50)
Servo.start(2)

def ServoDondur(Aci):
     Servo.ChangeDutyCycle(2+(Aci/180.0)*10)

while True:
    for i in range(0,180,5):
        ServoDondur(i)
        time.sleep(0.1)
    for i in range(0,180,5):
        ServoDondur(180-i)
        time.sleep(0.1)    