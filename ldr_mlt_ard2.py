from gpiozero import LightSensor, Buzzer

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(23,GPIO.OUT)
GPIO.setup(24,GPIO.OUT)
GPIO.setup(25,GPIO.OUT)
GPIO.setup(8,GPIO.OUT)


ldr = LightSensor(4)
ldr2 = LightSensor(17)
ldr3 = LightSensor(27)
ldr4 = LightSensor(22)


while True:


    bir=ldr.value+ldr2.value
    iki=ldr3.value+ldr4.value
    uc=ldr.value+ldr3.value 
    dort=ldr2.value+ldr4.value

    fark1=bir-iki;
    fark2=iki-bir;
    fark3=uc-dort;
    fark4=dort-uc;

    print("bir= %s" %bir)
    print("iki= %s" %iki)
    print("uc= %s" %uc)
    print("dort= %s" %dort)
    
    print("fark1= %s" %fark1)
    print("fark3= %s" %fark3)

    if bir>iki and fark1>0.01:
        GPIO.output(23,GPIO.HIGH)
        GPIO.output(25,GPIO.LOW)
        time.sleep(1)
    elif iki>bir and fark2>0.01:
    	GPIO.output(25,GPIO.HIGH)
        GPIO.output(23,GPIO.LOW)
        time.sleep(1)
    else :
        GPIO.output(25,GPIO.LOW)
        GPIO.output(23,GPIO.LOW)
        time.sleep(1)
        
    if uc>dort and fark3>0.01:
    	GPIO.output(24,GPIO.HIGH)
        GPIO.output(8,GPIO.LOW)
        time.sleep(1)
    elif dort>uc and fark4>0.01:
    	GPIO.output(8,GPIO.HIGH)
        GPIO.output(24,GPIO.LOW)
        time.sleep(1)    
    else :
        GPIO.output(24,GPIO.LOW)
        GPIO.output(8,GPIO.LOW)
        time.sleep(1)
     
      
