from gpiozero import LightSensor, Buzzer

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(23,GPIO.OUT)
GPIO.setup(24,GPIO.OUT)
GPIO.setup(25,GPIO.OUT)
GPIO.setup(8,GPIO.OUT)


ldr = LightSensor(4)
ldr2 = LightSensor(17)
ldr3 = LightSensor(27)
ldr4 = LightSensor(22)


while True:


    bir=ldr.value+ldr2.value
    iki=ldr3.value+ldr4.value
    uc=ldr.value+ldr3.value 
    dort=ldr2.value+ldr4.value

    print("ldr= %s" %ldr.value)
    print("ldr2= %s" %ldr2.value)
    print("ldr3= %s" %ldr3.value)
    print("ldr4= %s" %ldr4.value)
    print("bir= %s" %bir)
    print("iki= %s" %iki)
    print("uc= %s" %uc)
    print("dort= %s" %dort)
    
    if bir>iki:
        GPIO.output(23,GPIO.HIGH)
        GPIO.output(25,GPIO.LOW)
        time.sleep(1)
    elif iki>bir:
    	GPIO.output(25,GPIO.HIGH)
        GPIO.output(23,GPIO.LOW)
        time.sleep(1)
    if uc>dort:
    	GPIO.output(24,GPIO.HIGH)
        GPIO.output(8,GPIO.LOW)
        time.sleep(1)
    elif dort>uc:
    	GPIO.output(8,GPIO.HIGH)
        GPIO.output(24,GPIO.LOW)
        time.sleep(1)
    GPIO.output(25,GPIO.LOW)
    GPIO.output(23,GPIO.LOW)
    GPIO.output(24,GPIO.LOW)
    GPIO.output(8,GPIO.LOW)
      
